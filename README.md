# Description of these SVMs

```
v1 was trained with very few crop images of blot and text, so it's just a try result.

v2 was trained with much more data, with pretty good accuracy.

v3 was trained with some mistakes v2 made, with best performance.(Recommended)

v4 was trained for some special purpose: extract regions without non-max-suppression. Anyway it doesn't work well.
```
```
If you want to train SVM yourself, take a look at the script I wrote:
HaojieYuan/blot_cp_detector    LBP_SVM.py
which is available on github.
```
